import 'package:flutter/material.dart';

Color primaryBlack = Color(0xff202c3b);

class DataSource {
  static String quote =
      "Tidak ada dalam hidup yang perlu ditakuti, itu hanya untuk dipahami. Sekaranglah waktunya untuk lebih memahami, sehingga rasa takut kita berkurang.";

  static List questionAnswers = [
    {
      "pertanyaan": "Apa itu virus corona?",
      "jawaban":
          "Coronavirus adalah keluarga besar virus yang dapat menyebabkan penyakit pada hewan atau manusia. Pada manusia, beberapa virus corona diketahui menyebabkan infeksi saluran pernapasan mulai dari flu biasa hingga penyakit yang lebih parah seperti Middle East Respiratory Syndrome (MERS) dan Severe Acute Respiratory Syndrome (SARS). Virus corona yang paling baru ditemukan menyebabkan penyakit virus corona COVID-19."
    },
    {
      "pertanyaan": "Apa itu COVID-19?",
      "jawaban":
          "COVID-19 adalah penyakit menular yang disebabkan oleh virus korona yang paling baru ditemukan. Virus dan penyakit baru ini tidak diketahui sebelum wabah dimulai di Wuhan, China, pada Desember 2019."
    },
    {
      "pertanyaan": "Apa saja gejala COVID-19?",
      "jawaban":
          "Gejala COVID-19 yang paling umum adalah demam, kelelahan, dan batuk kering. Beberapa pasien mungkin mengalami sakit dan nyeri, hidung tersumbat, pilek, sakit tenggorokan atau diare. Gejala ini biasanya ringan dan dimulai secara bertahap. Beberapa orang terinfeksi tetapi tidak menunjukkan gejala apa pun dan tidak merasa tidak enak badan. Kebanyakan orang (sekitar 80%) sembuh dari penyakit tanpa memerlukan perawatan khusus. Sekitar 1 dari setiap 6 orang yang terkena COVID-19 menjadi sakit parah dan mengalami kesulitan bernapas. Orang tua, dan mereka yang memiliki masalah medis seperti tekanan darah tinggi, masalah jantung atau diabetes, lebih mungkin mengembangkan penyakit serius. Orang dengan demam, batuk, dan kesulitan bernapas harus mencari pertolongan medis."
    },
    {
      "pertanyaan": "Bagaimana COVID-19 menyebar?",
      "jawaban":
          "Orang dapat tertular COVID-19 dari orang lain yang memiliki virus. Penyakit ini dapat menyebar dari orang ke orang melalui tetesan kecil dari hidung atau mulut yang menyebar saat penderita COVID-19 batuk atau mengeluarkan napas. Tetesan ini mendarat di benda dan permukaan di sekitar orang tersebut. Orang lain kemudian tertular COVID-19 dengan menyentuh benda atau permukaan ini, kemudian menyentuh mata, hidung, atau mulut mereka. Orang juga dapat tertular COVID-19 jika mereka menghirup tetesan dari seseorang dengan COVID-19 yang batuk atau menghembuskan tetesan. Inilah mengapa penting untuk menjaga jarak lebih dari 1 meter (3 kaki) dari orang yang sedang sakit. \ nWHO sedang menilai penelitian yang sedang berlangsung tentang cara penyebaran COVID-19 dan akan terus membagikan temuan terbaru."
    },
    {
      "pertanyaan":
          "Apakah virus penyebab COVID-19 dapat ditularkan melalui udara?",
      "jawaban":
          "Studi hingga saat ini menunjukkan bahwa virus yang menyebabkan COVID-19 terutama ditularkan melalui kontak dengan tetesan pernapasan daripada melalui udara."
    },
    {
      "pertanyaan":
          "Bisakah COVID-19 ditularkan dari orang yang tidak memiliki gejala?",
      "jawaban":
          "Cara utama penyebaran penyakit ini adalah melalui tetesan pernapasan yang dikeluarkan oleh seseorang yang sedang batuk. Risiko tertular COVID-19 dari seseorang tanpa gejala sama sekali sangat rendah. Namun, banyak orang dengan COVID-19 hanya mengalami gejala ringan. Ini terutama benar pada tahap awal penyakit. Oleh karena itu, dimungkinkan untuk tertular COVID-19 dari seseorang yang, misalnya, hanya batuk ringan dan tidak merasa sakit. WHO sedang menilai penelitian yang sedang berlangsung tentang periode penularan COVID-19 dan akan terus membagikan temuan terbaru.    "
    },
    {
      "pertanyaan":
          "Bisakah saya tertular COVID-19 dari kotoran seseorang yang mengidap penyakit?",
      "jawaban":
          "Risiko tertular COVID-19 dari kotoran orang yang terinfeksi tampaknya rendah. Sementara penyelidikan awal menunjukkan bahwa virus mungkin ada dalam tinja dalam beberapa kasus, penyebaran melalui jalur ini bukanlah ciri utama wabah. WHO sedang menilai penelitian yang sedang berlangsung tentang cara penyebaran COVID-19 dan akan terus membagikan temuan baru. Karena ini berisiko, bagaimanapun, itu adalah alasan lain untuk membersihkan tangan secara teratur, setelah menggunakan kamar mandi dan sebelum makan."
    },
    {
      "pertanyaan": "Seberapa besar kemungkinan saya tertular COVID-19?",
      "jawaban":
          "Risikonya tergantung di mana Anda berada - dan lebih khusus lagi, apakah ada wabah COVID-19 yang sedang berlangsung di sana. \ NBagi kebanyakan orang di sebagian besar lokasi, risiko tertular COVID-19 masih rendah. Namun, sekarang ada tempat-tempat di seluruh dunia (kota atau daerah) dimana penyakit itu menyebar. Untuk orang yang tinggal di, atau mengunjungi, area ini, risiko tertular COVID-19 lebih tinggi. Pemerintah dan otoritas kesehatan mengambil tindakan tegas setiap kali kasus baru COVID-19 teridentifikasi. Pastikan untuk mematuhi batasan lokal apa pun tentang perjalanan, pergerakan, atau pertemuan besar. Bekerja sama dalam upaya pengendalian penyakit akan mengurangi risiko Anda tertular atau menyebarkan COVID-19. \ NPenularan COVID-19 dapat diatasi dan penularannya dihentikan, seperti yang telah ditunjukkan di China dan beberapa negara lain. Sayangnya, wabah baru bisa muncul dengan cepat. Penting untuk menyadari situasi tempat Anda berada atau akan pergi. WHO menerbitkan pembaruan harian tentang situasi COVID-19 di seluruh dunia."
    },
    {
      "pertanyaan": "Siapa yang berisiko terkena penyakit parah?",
      "jawaban":
          "Sementara kami masih belajar tentang bagaimana COVID-2019 memengaruhi orang, orang tua dan orang dengan kondisi medis yang sudah ada sebelumnya (seperti tekanan darah tinggi, penyakit jantung, penyakit paru-paru, kanker atau diabetes) tampaknya lebih sering mengembangkan penyakit serius daripada yang lain. "
    },
    {
      "pertanyaan":
          "Haruskah saya memakai masker untuk melindungi diri saya sendiri?",
      "jawaban":
          "Gunakan masker hanya jika Anda sakit dengan gejala COVID-19 (terutama batuk) atau merawat seseorang yang mungkin mengidap COVID-19. Masker wajah sekali pakai hanya dapat digunakan satu kali. Jika Anda tidak sakit atau merawat seseorang yang sakit maka Anda membuang-buang masker. Ada kekurangan masker di seluruh dunia, jadi WHO mendesak orang-orang untuk menggunakan masker dengan bijak. \ NWHO menyarankan penggunaan masker medis yang rasional untuk menghindari pemborosan sumber daya berharga yang tidak perlu dan penyalahgunaan masker \ nCara paling efektif untuk melindungi diri sendiri dan orang lain Menangkal COVID-19 adalah dengan sering membersihkan tangan, menutupi batuk dengan tekukan siku atau tisu dan menjaga jarak minimal 1 meter (3 kaki) dari orang yang batuk atau bersin."
    },
    {
      "pertanyaan":
          "Apakah antibiotik efektif dalam mencegah atau mengobati COVID-19?",
      "jawaban":
          "Tidak. Antibiotik tidak bekerja melawan virus, mereka hanya bekerja pada infeksi bakteri. COVID-19 disebabkan oleh virus, jadi antibiotik tidak berfungsi. Antibiotik tidak boleh digunakan sebagai sarana pencegahan atau pengobatan COVID-19. Obat ini hanya boleh digunakan sesuai petunjuk dokter untuk mengobati infeksi bakteri. "
    }
  ];
}
