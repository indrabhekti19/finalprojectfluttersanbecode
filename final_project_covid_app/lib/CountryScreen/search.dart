import 'package:flutter/material.dart';
import 'package:pie_chart/pie_chart.dart';

class Search extends SearchDelegate {
  final List countryList;

  Search(this.countryList);

  @override
  List<Widget> buildActions(BuildContext context) {
    return [
      IconButton(
        onPressed: () {
          query = '';
        },
        icon: Icon(Icons.clear),
      )
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
        onPressed: () {
          Navigator.pop(context);
        },
        icon: Icon(Icons.arrow_back_ios));
  }

  @override
  Widget buildResults(BuildContext context) {
    return Container();
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    final suggestionList = query.isEmpty
        ? countryList
        : countryList
            .where((element) =>
                element['country'].toString().toLowerCase().startsWith(query))
            .toList();

    return ListView.builder(
        itemCount: suggestionList.length,
        itemBuilder: (context, index) {
          return Card(
            child: Container(
              margin: EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
              height: 250,
              child: Column(
                children: [
                  Row(
                    children: [
                      Container(
                        margin: EdgeInsets.symmetric(horizontal: 10.0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              suggestionList[index]['country'],
                              style: TextStyle(fontWeight: FontWeight.bold),
                            ),
                            SizedBox(
                              height: 7,
                            ),
                            Image.network(
                              suggestionList[index]['countryInfo']['flag'],
                              height: 40,
                              width: 60,
                            )
                          ],
                        ),
                      ),
                      Expanded(
                        child: Container(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                'TERKONFIRMASI : ' +
                                    suggestionList[index]['cases'].toString(),
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    color: Colors.red),
                              ),
                              Text(
                                'KASUS AKTIF : ' +
                                    suggestionList[index]['active'].toString(),
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    color: Colors.blue),
                              ),
                              Text(
                                'SEMBUH : ' +
                                    suggestionList[index]['recovered']
                                        .toString(),
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    color: Colors.green),
                              ),
                              Text(
                                'MENINGGAL : ' +
                                    suggestionList[index]['deaths'].toString(),
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    color: Colors.grey),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Expanded(
                    child: suggestionList == null
                        ? Container()
                        : PieChart(
                            chartType: ChartType.ring,
                            chartRadius: MediaQuery.of(context).size.width / 4,
                            dataMap: {
                              'KASUS AKTIF':
                                  suggestionList[index]['active'].toDouble(),
                              'SEMBUH':
                                  suggestionList[index]['recovered'].toDouble(),
                              'MENINGGAL':
                                  suggestionList[index]['deaths'].toDouble(),
                            },
                            colorList: [
                              Colors.blue[400],
                              Colors.green[400],
                              Colors.grey[700],
                            ],
                            chartValuesOptions: ChartValuesOptions(
                              showChartValuesOutside: true,
                              showChartValuesInPercentage: true,
                            ),
                          ),
                  ),
                ],
              ),
            ),
          );
        });
  }
}
