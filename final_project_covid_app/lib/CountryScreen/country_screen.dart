import 'package:final_project_covid_app/CountryScreen/search.dart';
import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:pie_chart/pie_chart.dart';

class CountryScreen extends StatefulWidget {
  @override
  _CountryScreenState createState() => _CountryScreenState();
}

class _CountryScreenState extends State<CountryScreen> {
  List countryData;
  fetchCountryData() async {
    http.Response response = await http
        .get(Uri.parse('https://corona.lmao.ninja/v3/covid-19/countries'));
    setState(() {
      countryData = json.decode(response.body);
    });
  }

  @override
  void initState() {
    fetchCountryData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: [
          IconButton(
            onPressed: () {
              showSearch(context: context, delegate: Search(countryData));
            },
            icon: Icon(Icons.search),
          ),
        ],
        title: Text('Daftar Negara'),
      ),
      body: countryData == null
          ? Center(
              child: CircularProgressIndicator(),
            )
          : ListView.builder(
              itemBuilder: (context, index) {
                return Card(
                  child: Container(
                    margin:
                        EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
                    height: 250,
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Container(
                              margin: EdgeInsets.symmetric(horizontal: 10.0),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(
                                    countryData[index]['country'],
                                    style:
                                        TextStyle(fontWeight: FontWeight.bold),
                                  ),
                                  SizedBox(
                                    height: 7,
                                  ),
                                  Image.network(
                                    countryData[index]['countryInfo']['flag'],
                                    height: 40,
                                    width: 60,
                                  )
                                ],
                              ),
                            ),
                            Expanded(
                              child: Container(
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text(
                                      'TERKONFIRMASI : ' +
                                          countryData[index]['cases']
                                              .toString(),
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          color: Colors.red),
                                    ),
                                    Text(
                                      'KASUS AKTIF : ' +
                                          countryData[index]['active']
                                              .toString(),
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          color: Colors.blue),
                                    ),
                                    Text(
                                      'SEMBUH : ' +
                                          countryData[index]['recovered']
                                              .toString(),
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          color: Colors.green),
                                    ),
                                    Text(
                                      'MENINGGAL : ' +
                                          countryData[index]['deaths']
                                              .toString(),
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          color: Colors.grey),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Expanded(
                          child: countryData == null
                              ? Container()
                              : PieChart(
                                  chartType: ChartType.ring,
                                  chartRadius:
                                      MediaQuery.of(context).size.width / 4,
                                  dataMap: {
                                    'KASUS AKTIF':
                                        countryData[index]['active'].toDouble(),
                                    'SEMBUH': countryData[index]['recovered']
                                        .toDouble(),
                                    'MENINGGAL':
                                        countryData[index]['deaths'].toDouble(),
                                  },
                                  colorList: [
                                    Colors.blue[400],
                                    Colors.green[400],
                                    Colors.grey[700],
                                  ],
                                  chartValuesOptions: ChartValuesOptions(
                                    showChartValuesOutside: true,
                                    showChartValuesInPercentage: true,
                                  ),
                                ),
                        ),
                      ],
                    ),
                  ),
                );
              },
              itemCount: countryData == null ? 0 : countryData.length,
            ),
    );
  }
}
