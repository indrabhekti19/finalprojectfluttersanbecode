import 'package:final_project_covid_app/HomeScreen/home_screen.dart';
import 'package:final_project_covid_app/LandingScreen/Provider/google_sign_in.dart';
import 'package:final_project_covid_app/LandingScreen/Widget/background_painter.dart';
import 'package:final_project_covid_app/LandingScreen/Widget/logged_in_widget.dart';
import 'package:final_project_covid_app/LandingScreen/Widget/sign_up_widget.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) => Scaffold(
        body: StreamBuilder(
          stream: FirebaseAuth.instance.authStateChanges(),
          builder: (context, snapshot) {
            final provider = Provider.of<GoogleSignInProvider>(context);

            if (provider.isSigningIn) {
              return buildLoading();
            } else if (snapshot.hasData) {
              return LoggedInWidget();
            } else {
              return SignUpWidget();
            }
          },
        ),
      );

  Widget buildLoading() => Stack(
        fit: StackFit.expand,
        children: [
          CustomPaint(painter: BackgroundPainter()),
          Center(child: CircularProgressIndicator()),
          HomeScreen(),
        ],
      );
}
