import 'package:final_project_covid_app/LandingScreen/Page/auth_page.dart';
import 'package:final_project_covid_app/LandingScreen/Widget/background_painter.dart';
import 'package:final_project_covid_app/LandingScreen/Widget/google_signup_button_widget.dart';
import 'package:flutter/material.dart';

class SignUpWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) => Stack(
        fit: StackFit.expand,
        children: [
          CustomPaint(painter: BackgroundPainter()),
          buildSignUp(context),
        ],
      );

  Widget buildSignUp(BuildContext context) => Column(
        children: [
          Spacer(),
          Align(
            alignment: Alignment.centerLeft,
            child: Container(
              margin: EdgeInsets.symmetric(horizontal: 30, vertical: 50),
              width: 175,
              child: Text(
                'Welcome Back To MyApp',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 30,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ),
          Spacer(),
          Text('Login untuk melanjutkan'),
          SizedBox(height: 12),
          OutlinedButton.icon(
            label: Text(
              'Masuk dengan Email',
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
            ),
            icon: Icon(
              Icons.email_outlined,
              color: Colors.red,
            ),
            onPressed: () => Navigator.of(context).push(
              MaterialPageRoute(builder: (context) => AuthPage()),
            ),
          ),
          SizedBox(height: 12),
          GoogleSignupButtonWidget(),
          Spacer()
        ],
      );
}
