import 'dart:convert';

import 'package:final_project_covid_app/CountryScreen/country_screen.dart';
import 'package:final_project_covid_app/DrawerScreen/DrawerScreen.dart';
import 'package:final_project_covid_app/PageTransition/slide.dart';
import 'package:final_project_covid_app/Panels/most_affected_country.dart';
import 'package:final_project_covid_app/Panels/panel_world_wide.dart';
import 'package:final_project_covid_app/ThemeProvider/change_theme_button.dart';
import 'package:final_project_covid_app/data_source.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:pie_chart/pie_chart.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomeScreen> {
  Map worldData;
  fetchWorldWideData() async {
    http.Response response =
        await http.get(Uri.parse('https://corona.lmao.ninja/v3/covid-19/all'));
    setState(() {
      worldData = json.decode(response.body);
    });
  }

  List countryData;
  fetchCountryData() async {
    http.Response response = await http.get(Uri.parse(
        'https://corona.lmao.ninja/v3/covid-19/countries?sort=deaths'));
    setState(() {
      countryData = json.decode(response.body);
    });
  }

  Future fetchData() async {
    fetchWorldWideData();
    fetchCountryData();
    print('fetchData called');
  }

  @override
  void initState() {
    fetchData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: [
          ChangeThemeButtonWidget(),
        ],
        centerTitle: true,
        title: Text("DATA COVID-19"),
      ),
      drawer: DrawerScreen(),
      body: RefreshIndicator(
        onRefresh: fetchData,
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                height: 100,
                alignment: Alignment.center,
                padding: EdgeInsets.all(10),
                color: Colors.orange[100],
                child: Text(
                  DataSource.quote,
                  style: TextStyle(
                      color: Colors.orange[800],
                      fontWeight: FontWeight.bold,
                      fontSize: 16),
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(
                    vertical: 10.0, horizontal: 10.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'Data Global',
                      style:
                          TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
                    ),
                    GestureDetector(
                      onTap: () {
                        Navigator.push(
                            context, SlideLeftRoute(page: CountryScreen()));
                      },
                      child: Container(
                        padding: EdgeInsets.all(10),
                        decoration: BoxDecoration(
                            color: primaryBlack,
                            borderRadius: BorderRadius.circular(15)),
                        child: Text(
                          'Regional',
                          style: TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.bold,
                              color: Colors.white),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              worldData == null
                  ? Center(child: CircularProgressIndicator())
                  : PanelWorldWide(
                      worldData: worldData,
                    ),
              worldData == null
                  ? Container()
                  : PieChart(
                      dataMap: {
                        'TERKONFIRMASI': worldData['cases'].toDouble(),
                        'KASUS AKTIF': worldData['active'].toDouble(),
                        'SEMBUH': worldData['recovered'].toDouble(),
                        'MENINGGAL': worldData['deaths'].toDouble(),
                      },
                      colorList: [
                        Colors.red[400],
                        Colors.blue[400],
                        Colors.green[400],
                        Colors.grey[700],
                      ],
                      chartValuesOptions: ChartValuesOptions(
                        showChartValuesInPercentage: true,
                      ),
                    ),
              SizedBox(
                height: 5,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(
                    horizontal: 10.0, vertical: 10.0),
                child: Center(
                  child: Text(
                    '10 Negara Paling Terdampak',
                    style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
                  ),
                ),
              ),
              SizedBox(
                height: 5,
              ),
              countryData == null
                  ? Container()
                  : MostAffectedPanel(
                      countryData: countryData,
                    ),
              SizedBox(
                height: 10,
              ),
              Center(
                child: Text(
                  ' === BERSAMA KITA LAWAN COVID-19 === ',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
