import 'package:final_project_covid_app/LandingScreen/Page/home_page.dart';
import 'package:final_project_covid_app/LandingScreen/Provider/email_sign_in.dart';
import 'package:final_project_covid_app/LandingScreen/Provider/google_sign_in.dart';
import 'package:final_project_covid_app/ThemeProvider/theme_provider.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
    statusBarColor: Colors.transparent,
  ));
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) => ChangeNotifierProvider(
        create: (context) => ThemeProvider(),
        builder: (context, _) {
          final themeProvider = Provider.of<ThemeProvider>(context);
          return MultiProvider(
            providers: [
              ChangeNotifierProvider(
                  create: (context) => GoogleSignInProvider()),
              ChangeNotifierProvider(create: (context) => EmailSignInProvider())
            ],
            child: MaterialApp(
              debugShowCheckedModeBanner: false,
              themeMode: themeProvider.themeMode,
              theme: MyThemes.lightTheme,
              darkTheme: MyThemes.darkTheme,
              home: HomePage(),
            ),
          );
        },
      );
}
