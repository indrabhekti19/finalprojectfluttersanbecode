import 'package:final_project_covid_app/FaqScreen/faq_screen.dart';
import 'package:final_project_covid_app/LandingScreen/Provider/google_sign_in.dart';
import 'package:final_project_covid_app/PageTransition/slide.dart';
import 'package:final_project_covid_app/ProfileScreen/profile_screen.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

class DrawerScreen extends StatefulWidget {
  @override
  _DrawerScreenState createState() => _DrawerScreenState();
}

class _DrawerScreenState extends State<DrawerScreen> {
  @override
  Widget build(BuildContext context) {
    final user = FirebaseAuth.instance.currentUser;
    return Drawer(
      child: ListView(
        children: [
          UserAccountsDrawerHeader(
            accountName: Text(
              'Indra Bhekti Utomo',
              style: TextStyle(fontSize: 18),
            ),
            currentAccountPicture: CircleAvatar(
              backgroundImage: AssetImage('assets/img/person.png'),
              // backgroundImage: NetworkImage(
              //         'https://upload.wikimedia.org/wikipedia/commons/thumb/7/7e/Circle-icons-profile.svg/1024px-Circle-icons-profile.svg.png') ??
              //     AssetImage('assets/img/loading.png'),
            ),
            accountEmail: Text('indrabhektiutomo@gmail.com'),
          ),
          DrawerListTile(
            iconData: Icons.person,
            title: "Tentang Saya",
            onTilePressed: () {
              Navigator.pop(context);
              // Navigator.push(
              //   context,
              //   PageRouteBuilder(
              //       transitionDuration: Duration(seconds: 1),
              //       transitionsBuilder:
              //           (context, animation, animationTime, child) {
              //         animation = CurvedAnimation(
              //             parent: animation, curve: Curves.elasticInOut);
              //         return ScaleTransition(
              //           scale: animation,
              //           alignment: Alignment.topCenter,
              //           child: child,
              //         );
              //       },
              //       pageBuilder: (context, animation, animationTime) {
              //         return ProfileScreen();
              //       }),
              // );
              Navigator.push(context, SlideLeftRoute(page: ProfileScreen()));
            },
          ),
          DrawerListTile(
            iconData: Icons.account_balance_wallet,
            title: "Donasi (Website WHO)",
            onTilePressed: () {
              Navigator.pop(context);
              launch(
                  'https://www.who.int/emergencies/diseases/novel-coronavirus-2019/donate');
            },
          ),
          DrawerListTile(
            iconData: Icons.web_asset,
            title: "Myth Busters (Website WHO)",
            onTilePressed: () {
              Navigator.pop(context);
              launch(
                  'https://www.who.int/emergencies/diseases/novel-coronavirus-2019/advice-for-public/myth-busters?gclid=Cj0KCQjwhr2FBhDbARIsACjwLo3RiQra-PXb-UNddVKVEAdgn-kTvhfVA8NvB-1WYIsWLBN3SJ8DpLgaAkxPEALw_wcB');
            },
          ),
          DrawerListTile(
            iconData: Icons.question_answer,
            title: "FAQs",
            onTilePressed: () {
              Navigator.pop(context);
              Navigator.push(context, SlideLeftRoute(page: FaqScreen()));
            },
          ),
          DrawerListTile(
            iconData: Icons.logout,
            title: "Logout",
            onTilePressed: () {
              final provider =
                  Provider.of<GoogleSignInProvider>(context, listen: false);
              provider.logout();
            },
          ),
        ],
      ),
    );
  }
}

class DrawerListTile extends StatelessWidget {
  final IconData iconData;
  final String title;
  final VoidCallback onTilePressed;

  const DrawerListTile({Key key, this.iconData, this.title, this.onTilePressed})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: onTilePressed,
      dense: true,
      leading: Icon(iconData),
      title: Text(
        title,
        style: TextStyle(fontSize: 16),
      ),
    );
  }
}
